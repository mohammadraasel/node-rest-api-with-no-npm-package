const http = require('http');
const https = require('https');
const url = require('url')
const fs = require('fs')
const StringDecoder = require('string_decoder').StringDecoder

const  router  = require('./routes')
const config = require('./config')

const httpServer = http.createServer(function (req, res) {
	unifiedServer(req, res)
})

httpServer.listen(config.httpPort, function () {
	console.log(`The server is running on port:${config.httpPort} in ${config.name} mode.`)
})


const httpsOptions = {
	key: fs.readFileSync('./https/key.pem'),
	cert: fs.readFileSync('./https/cert.pem')
}
const httpsServer = https.createServer(httpsOptions, function (req, res) {
	unifiedServer(req, res)
})

httpsServer.listen(config.httpsPort, function () {
	console.log(`The server is running on port:${config.httpsPort} in ${config.name} mode.`)
})

 

const unifiedServer = function (req, res) {
	const parsedUrl = url.parse(req.url, true)
	const pathname = parsedUrl.pathname.replace(/^\/+|\/+$/g, '')
	const queryStringObject = parsedUrl.query
	const method = req.method
	const headers = req.headers
	const decoder = new StringDecoder('utf-8')
	let buffer = ''

	req.on('data', function (data) {
		buffer += decoder.write(data)
	})

	req.on('end', function () {
		buffer += decoder.end()

		const selectedHandler = router[pathname] ? router[pathname] : router.notFound 
		const data = {
			pathname,
			headers,
			method,
			queryStringObject,
			payload: buffer !== '' ? JSON.parse(buffer) : {}
		}

		selectedHandler(data, function (statusCode, payload) {
			statusCode = typeof statusCode === 'number' ? statusCode : 200
			payload  = typeof payload === 'object' ? payload : {}
			
			const payloadString = JSON.stringify(payload)
			res.setHeader('Content-Type', 'application/json')
			res.writeHead(statusCode) 
			res.end(payloadString)
		})	
	})
}


