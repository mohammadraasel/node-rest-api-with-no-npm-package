const commonController = require('../controllers')


const router = {}

for (let [key, value] of Object.entries({ ...commonController })){
	router[key] = value
}

module.exports = router