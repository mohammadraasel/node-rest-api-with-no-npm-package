
exports.ping = function (data, callback) {
	callback(200, { message: 'Server is alive.' })
}

exports.notFound = function (data, callback) {
	callback(404, { message: 'Route not found.' })
}

