

const environments = {}

environments.development = {
	httpPort: 3000,
	httpsPort: 3001,
	name: 'development'
}

environments.staging = {
	httpPort: 4000,
	httpsPort: 4001,
	name: 'staging'
}

environments.production = {
	httpPort: 5000,
	httpsPort: 5001,
	name: 'production'
}

const currentEnvironment = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : ''

const environmentToExport = environments[currentEnvironment] ? environments[currentEnvironment] : environments.development

module.exports = environmentToExport